# first stage of building angular image
FROM node:lts-alpine3.15 as build
RUN mkdir -p /app
WORKDIR /app

COPY package.json /app/
COPY package-lock.json /app/
RUN npm install

COPY . /app/
RUN npm run build --prod

# final stage
FROM nginx:alpine
COPY --from=build /app/dist/StratisFullNodeWebApp /usr/share/nginx/html
